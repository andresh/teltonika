require("luci.fs")
require("luci.config")
require "teltonika_lua_functions"

local utl = require "luci.util"
local nw = require "luci.model.network"
local sys = require "luci.sys"


m = Map("openvpn", translate("RMS Settings"), translate(""))
m.wizStep = 99

s = m:section(NamedSection, "teltonika_auth_service", "openvpn", translate("Remote Management System"))
open_e = s:option(Flag, "enable", translate("Enable remote management"), translate(""))
open_e.rmempty = false
function open_e.cfgvalue(self, section)
	local RuleEn = ""
	RuleEn =  luci.util.trim(luci.sys.exec("uci -q get rms_connect.rms_connect.enable"))
	if RuleEn == "1" then
		return self.enabled
	else
		return self.disabled
	end

end

function open_e.write(self, section, value)
	if value then
		m.uci:set("rms_connect", "rms_connect", "enable", value)
		m.uci:commit("rms_connect")
	end
end

s = m:section(NamedSection, "", "", translate(""));
s.template = "cfgwzd-module/monitoring"


if m:formvalue("cbi.wizard.finish") then
	luci.http.redirect(luci.dispatcher.build_url("/admin/status/overview"))
end

if m:formvalue("cbi.wizard.skip") then
	luci.http.redirect(luci.dispatcher.build_url("/admin/status/overview"))
end

function m.on_commit()
	luci.sys.call("/etc/init.d/rms_connect restart")
end

return m
