require("luci.fs")
require("luci.config")
require "teltonika_lua_functions"

local utl = require "luci.util"
local nw = require "luci.model.network"
local sys = require "luci.sys"


m = Map("openvpn", translate("RMS Settings"), translate(""))

s = m:section(NamedSection, "teltonika_auth_service", "openvpn", translate("Remote Management System"))
open_e = s:option(Flag, "enable", translate("Enable remote monitoring"), translate(""))
open_e.rmempty = false
function open_e.cfgvalue(self, section)
	local RuleEn = ""
	RuleEn =  luci.util.trim(luci.sys.exec("uci -q get rms_connect.rms_connect.enable"))
                if RuleEn == "1" then
                        return self.enabled
                else
                        return self.disabled
                end

end

function open_e.write(self, section, value)
        if value then
                m.uci:set("rms_connect", "rms_connect", "enable", value)
                m.uci:commit("rms_connect")
        end
end

o = s:option(Value, "remote", translate("Hostname"))
o.datatype = "or(hostname,ip4addr)"
o.default = "rms.teltonika.lt"

p = s:option(Value, "port", translate("Port"), translate("Port number."))
p.datatype = "port"
p.default = "5002"
function p.write(self, section, value)
        if value then
                m.uci:set("rms_connect", "rms_connect", "port", value)
                m.uci:commit("rms_connect")
        end
end
function p.cfgvalue(self, section)
        local RuleEn = ""
        return luci.util.trim(luci.sys.exec("uci -q get rms_connect.rms_connect.port"))
	
end


s = m:section(NamedSection, "", "", translate(""));
s.template = "admin_system/netinfo_monitoring"

return m
