	m = Map("mqtt_pub", translate("MQTT Publisher"), translate(""))
	
	local s2 = m:section(NamedSection, "mqtt_pub", "mqtt_pub",  translate(""), "")                                                 
	
	enabled_pub = s2:option(Flag, "enabled", "Enable", "Select to enable MQTT publisher")
	
	remote_addr = s2:option(Value, "remote_addr", "Hostname", "Specify address of the broker")
	remote_addr:depends("enabled", "1")
	remote_addr.datatype = "host"
	
	remote_port = s2:option(Value, "remote_port", "Port", "Specify port of the broker")
	remote_port:depends("enabled", "1")
	remote_port.datatype = "port"
	
	remote_username = s2:option(Value, "username", "Username", "Specify username of remote host")
	remote_username:depends("enabled", "1")

	remote_password = s2:option(Value, "password", "Password", "Specify password of remote host")
	remote_password:depends("enabled", "1")
	remote_password.password = true
	
	return m	
