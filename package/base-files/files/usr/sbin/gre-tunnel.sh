#!/bin/sh

. /lib/teltonika-functions.sh

ENABLED="$1"
if [ "$ENABLED" = "1" ]; then
	# Wait for WAN interface to get an IP address.
	wan_ip=`tlt_wait_for_wan gre-tunnel`
	ifname="$2"
	remote_ip="$3"
	remote_network="$4"
	remote_netmask="$5"
	tunnel_ip="$6"
	tunnel_netmask="$7"
	tunnel_network=`ipcalc.sh $tunnel_ip $tunnel_netmask | grep NETWORK | cut -d= -f2`

	ttl="$8"
	pmtud="$9"
	mtu="$10"

	keepalive="$11"
	keepalive_host="$12"
	keepalive_interval="$13"
	redirect_lan=$(uci -q get gre_tunnel.$ifname.lan_to_gre)
	sim=$(uci -q get gre_tunnel.$ifname.sim)
	sim_in_use=$(sim_switch sim)

	echo "$ifname $remote_ip $wan_ip $tunnel_ip"

	if [ -n "$sim" -a "$sim" != "$sim_in_use" ]; then
		logger -t "GRE-TUN" "${ifname} tunnel do not match sim card slot."
		exit 0
	fi

	if [ -n "$ifname" -a -n "$remote_ip" -a -n "$wan_ip" -a -n "$tunnel_ip" ]; then
		error=0
		logger "[GRE-TUN] ${ifname} Setuping new tunnel..."
		if [ "$pmtud" = "1" ]; then
			ip tunnel add "$ifname" mode gre remote "$remote_ip" local "$wan_ip" nopmtudisc
		else
			ip tunnel add "$ifname" mode gre remote "$remote_ip" local "$wan_ip" ttl "$ttl"
		fi
		error=`expr $error + $?`
		ifconfig "$ifname" up
		error=`expr $error + $?`
		ifconfig "$ifname" "$tunnel_ip"
		error=`expr $error + $?`
		ifconfig "$ifname" pointopoint 0.0.0.0
		error=`expr $error + $?`

		if [ "$mtu" -ne 0 ]; then
			ip link set "$ifname" mtu "$mtu"
		fi

		ip route add "$remote_network"/"$remote_netmask" dev "$ifname"
		error=`expr $error + $?`
		ip route add "$tunnel_network"/"$tunnel_netmask" dev "$ifname"
		error=`expr $error + $?`
		if [ "$error" -eq 0 ]; then
			logger -t "GRE-TUN" "${ifname} Started successful."
			/sbin/chroutes
			sleep 5
			ping "$remote_network" -c 3
			sleep 1

			#pridetas funkcionalumas, kad butu nukreipiamas lan trafficas i gre tuneli. Sukuriama nauja routing table, poto jai priskiriamas lan, ir nurodomas default gw i gre.
			if [ "$redirect_lan" = "1" ]; then

				lan_ip=$(uci -q get network.lan.ipaddr)
				lan_netmask=$(uci -q get network.lan.netmask)
				lan_ip=$(ipcalc.sh $lan_ip $lan_netmask | grep NETWORK | cut -d= -f2)
				lan_netmask=$(ipcalc.sh $lan_ip $lan_netmask | grep PREFIX | cut -d= -f2)
				grep -q "10 vpn" /etc/iproute2/rt_tables
				table_found="$?"
				if [ "$table_found" = "1" ]; then
					echo "10 vpn" >> /etc/iproute2/rt_tables
				fi

				ip rule add from $lan_ip/$lan_netmask priority 9 table vpn
				ip route add $lan_ip/$lan_netmask dev $ifname table vpn
				ip route replace default dev $ifname table vpn
				ip route flush cache
				logger -t "GRE-TUN" "LAN redirected to ${ifname}."
			else
				logger -t "GRE-TUN" "LAN redirection is not set."
			fi

		else
			logger -t "GRE-TUN" "${ifname} Error on setuping new tunnel."
		fi
	else
		logger -t "GRE-TUN" "${ifname} error: Tunnel not created (Remote_ip=${remote_ip}, WAN_ip=${wan_ip}, Tunnel_ip=${tunnel_ip})"
	fi
fi
