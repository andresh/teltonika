#!/bin/sh

l2tp_file="/tmp/l2tp_watchdog"
wait_timeout="2"

if [ ! -f "$l2tp_file" ]; then
	echo "1" > $l2tp_file
fi

retry_count=`cat $l2tp_file`

if [ $retry_count -gt $wait_timeout ]; then
    logger -t l2tp_watchdog.sh "Retry $retry_count of $wait_timeout.Reloading network"
    /etc/init.d/network restart
    rm $l2tp_file
else
    logger -t l2tp_watchdog.sh "Retry $retry_count of $wait_timeout. Waiting for connection."
    retry_count=$(($retry_count + 1))
    echo "$retry_count" > $l2tp_file
fi