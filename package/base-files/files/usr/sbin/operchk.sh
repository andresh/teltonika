#!/bin/sh

while [ true ]; do
	cgreg=`gsmctl -A AT+CGREG?`
	num=0

	for i in $(echo $cgreg | tr "," "\n"); do
		if [ "$num" -eq 2 ] ; then
			if [ "$i" != "5" ] && [ "$i" != "1" ]; then
				logger operchk: lost connection
				/etc/init.d/operctl restart
				exit
			fi
		fi
		num=$((num + 1))
	done

	sleep 30
done