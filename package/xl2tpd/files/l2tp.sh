#!/bin/sh

[ -x /usr/sbin/xl2tpd ] || exit 0

[ -n "$INCLUDE_ONLY" ] || {
	. /lib/functions.sh
	. ../netifd-proto.sh
	init_proto "$@"
}

proto_l2tp_init_config() {
	proto_config_add_string "username"
	proto_config_add_string "password"
	proto_config_add_string "keepalive"
	proto_config_add_string "pppd_options"
	proto_config_add_boolean "ipv6"
	proto_config_add_int "mtu"
	proto_config_add_int "checkup_interval"
	proto_config_add_string "server"
	available=1
	no_device=1
	no_proto_task=1
}

proto_l2tp_setup() {
	local interface="$1"
	local optfile="/tmp/l2tp/options.${interface}"
	local ip serv_addr server host

	json_get_var server server
	host="${server%:*}"
	for ip in $(resolveip -t 5 "$host"); do
		( proto_add_host_dependency "$interface" "$ip" )
		serv_addr=1
	done
	[ -n "$serv_addr" ] || {
		echo "Could not resolve server address" >&2
		sleep 5
		proto_setup_failed "$interface"
		exit 1
	}

	# workaround del l2tp su multiwan, netifd sudeda static routes per kuriuos pasiekiamas l2tp tunelis
        # ir perejus i backupWAN neistrina mainWAN static route i tuneli del ko nebeveikia l2tp tunelis su backupWAN ir atvirksciai.
        multiwan=`uci -q get multiwan.config.enabled`      

         if [ "$multiwan" == "1" ]; then
            l2tp_file="/tmp/l2tp_serverip"
            l2tp_serverip_to_file=`uci show network | grep server | awk -F '=' '{ print $2 }' > $l2tp_file`
            while read -r line
                do
                    route del $line 2> /dev/null
                    route del $line 2> /dev/null
                done < $l2tp_file
        fi

        local ipsec_mode=`uci show strongswan | grep transport`
        local ipsec_transport_section=`uci show strongswan | grep transport | awk -F '.' '{ print $2 }' | tail -n1`
        local ipsec_transport_enable=`uci -q get strongswan.$ipsec_transport_section.enabled`
        if [ -n "$ipsec_mode" -a "$ipsec_transport_enable" == "1" ]; then
            primary_wan=`route | grep default | head -n1 | awk '{ print $8}'`
            ip_addr=`ifconfig $primary_wan | grep "inet addr" | awk '{ print $2}' | awk -F':' '{print $2}'`
            echo "IPSec transport mode enabled."
            local wait_timeout=0
            while [ -z "$(ipsec status | grep "ESTABLISHED")" ] || [ -z "$(ipsec status | grep "ESTABLISHED" | grep "$ip_addr")" ]; do
                wait_timeout=$(($wait_timeout + 1))
                echo "Establishing connection. Retry $wait_timeout of 5"
                if [ "$wait_timeout" -ge 5 ]; then
                        echo "Can't establish connection. Reloading..." >&2
                        /etc/init.d/ipsec restart
                        exit 1
                fi
                sleep 3
            done
            echo "IPsec tunnel established, connecting to L2TP server..."
        fi

	# Start and wait for xl2tpd
	if [ ! -p /var/run/xl2tpd/l2tp-control -o -z "$(pidof xl2tpd)" ]; then
		/etc/init.d/xl2tpd restart

		local wait_timeout=0
		while [ ! -p /var/run/xl2tpd/l2tp-control ]; do
			wait_timeout=$(($wait_timeout + 1))
			[ "$wait_timeout" -gt 5 ] && {
				echo "Cannot find xl2tpd control file." >&2
				proto_setup_failed "$interface"
				exit 1
			}
			sleep 1
		done
	fi

	local ipv6 demand keepalive username password pppd_options mtu
	json_get_vars ipv6 demand keepalive username password pppd_options mtu
	[ "$ipv6" = 1 ] || ipv6=""
	if [ "${demand:-0}" -gt 0 ]; then
		demand="precompiled-active-filter /etc/ppp/filter demand idle $demand"
	else
		demand="persist"
	fi

	local interval="${keepalive##*[, ]}"
	[ "$interval" != "$keepalive" ] || interval=5

	keepalive="${keepalive:+lcp-echo-interval $interval lcp-echo-failure ${keepalive%%[, ]*}}"
	username="${username:+user \"$username\" password \"$password\"}"
	ipv6="${ipv6:++ipv6}"
	mtu="${mtu:+mtu $mtu mru $mtu}"

	mkdir -p /tmp/l2tp
	cat <<EOF >"$optfile"
usepeerdns
nodefaultroute
ipparam "$interface"
ifname "l2tp-$interface"
ip-up-script /lib/netifd/ppp-up
ipv6-up-script /lib/netifd/ppp-up
ip-down-script /lib/netifd/ppp-down
ipv6-down-script /lib/netifd/ppp-down
# Don't wait for LCP term responses; exit immediately when killed.
lcp-max-terminate 0
$keepalive
$username
$ipv6
$mtu
$pppd_options
EOF

	xl2tpd-control add l2tp-${interface} pppoptfile=${optfile} lns=${server} || {
		echo "xl2tpd-control: Add l2tp-$interface failed" >&2
		proto_setup_failed "$interface"
		exit 1
	}
	xl2tpd-control connect l2tp-${interface} || {
		echo "xl2tpd-control: Connect l2tp-$interface failed" >&2
		proto_setup_failed "$interface"
		exit 1
	}
}

proto_l2tp_teardown() {
	local interface="$1"
	local optfile="/tmp/l2tp/options.${interface}"

	rm -f ${optfile}
	if [ -p /var/run/xl2tpd/l2tp-control ]; then
		xl2tpd-control remove l2tp-${interface} || {
			echo "xl2tpd-control: Remove l2tp-$interface failed" >&2
		}
	fi
	# Wait for interface to go down
        while [ -d /sys/class/net/l2tp-${interface} ]; do
		sleep 1
	done
}

[ -n "$INCLUDE_ONLY" ] || {
	add_protocol l2tp
}
